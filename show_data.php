<?php
    $DB_NAME = "db_penyewaan";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $nama = mysqli_real_escape_string($conn,trim($_POST["nama"]));
        $sql = "SELECT m.id_member,m.nama,m.telepon,m.level,concat('http://192.168.43.85/0b_web/images/',m.photos) as url
		FROM member m
        WHERE m.nama like '%$nama%'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_member = array();
            while($member = mysqli_fetch_assoc($result)){
                array_push($data_member,$member);
            }
            echo json_encode($data_member);
        }
    }
?>