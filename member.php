<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Member</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Penyewaan</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link active" href="member.php">Member <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="about.php">About</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h4 class="mt-3 mb-3">Daftar Member</h4>
    <a href="insert.php" class="btn btn-primary mb-3">
      Tambah Member
    </a>
    <?php
    if(isset($_GET['pesan'])){
        if($_GET['pesan'] == "insertsuccess"){
            $msg = "sukses ditambahkan";
            $tipe = "success";
        }else if($_GET['pesan'] == "updatesuccess"){
            $msg = "sukses diedit";
            $tipe = "success";
        }else if($_GET['pesan'] == "deletesuccess"){
            $msg = "sukses dihapus";
            $tipe = "success";
        }else if($_GET['pesan'] == "insertfailed"){
            $msg = "gagal ditambahkan";
            $tipe = "danger";
        }else if($_GET['pesan'] == "updatefailed"){
            $msg = "gagal diedit";
            $tipe = "danger";
        }else if($_GET['pesan'] == "deletefailed"){
            $msg = "gagal dihapus";
            $tipe = "danger";
        }
        echo '<div class="alert alert-'.$tipe.' alert-dismissible fade show" role="alert">
                Data Member <strong>'.$msg.'</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
    ?>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">ID Member</th>
        <th scope="col">Nama</th>
        <th scope="col">Telepon</th>
        <th scope="col">Level</th>
        <th scope="col">Foto</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($db->tampildata() as $mem) : ?>
        <tr>
            <td><?= $mem['id_member'] ?></td>
            <td><?= $mem['nama'] ?></td>
            <td><?= $mem['telepon'] ?></td>
            <td><?php
            if($mem['level']=='Silver'){
                echo "<img src='res/silver.png' width='80px' height='30px' />";
            }else if($mem['level']=='Gold'){
                echo "<img src='res/gold.png' width='80px' height='30px' />";
            }else if($mem['level']=='Admin'){
                echo "<img src='res/admin.png' width='80px' height='50px' />";
            }
              
             ?></td>
            <td><img src="<?= $mem['url'] ?>" width="40px" height="50px" /></td>
            <td>
                <a href="update.php?id_member=<?php echo $mem['id_member']; ?>" class="btn btn-warning">Edit</a>
                <a href="proses.php?id_member=<?php echo $mem['id_member']; ?>&aksi=m_delete" class="btn btn-danger">Hapus</a>
			</td>
        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>