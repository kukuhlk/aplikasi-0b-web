<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Member</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="#">Penyewaan</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="#">Home</a>
                <a class="nav-item nav-link" href="member.php">Member</span></a>
                <a class="nav-item nav-link" href="about.php">About <span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4">Selamat datang di Penyewaan</h1>
            <p class="lead">Toko Penyewaan Property</p>
            <hr class="my-4">
            <p>Sebuah persewaan dengan harga di bawah rata rata, karena kami selalu membantu orang yang membutuhkan barang kami</p>
            <a class="btn btn-primary btn-lg" href="about.php" role="button">Lebih Lanjut</a>
        </div>
    </div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>