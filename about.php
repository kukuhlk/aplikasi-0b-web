<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman About Penyewaan</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Penyewaan</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="member.php">Member</span></a>
                <a class="nav-item nav-link active" href="#">About <span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
        <div class="card mt-3">
            <div class="card-header">
                About Page
            </div>
            <div class="card-body">
                <h5 class="card-title">Penyewaan</h5>
                <p class="card-text">adalah sebuah persetujuan di mana sebuah pembayaran dilakukan atas penggunaan suatu barang atau properti secara sementara oleh orang lain.
                 Barang yang dapat disewa bermacam-macam, tarif dan lama sewa juga bermacam-macam. Rumah umumnya disewa dalam satuan tahun, mobil dalam satuan hari, 
                 permainan komputer seperti PlayStation disewa dalam satuan jam. Untuk sewa mobil, biasanya perusahaan jasa penyewaa mobil menerapkan tarif per 12 jam 
                 atau per 24 jam.</p>
                Dibuat oleh :
                <ul>
                    <li>Kukuh Lutfi Kurniawan - 1931733072</li>
                    <li>Teddy Harfa As'ad Sunaryo - 1931733084</li>
                </ul>
                
            </div>
        </div>
    </div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
