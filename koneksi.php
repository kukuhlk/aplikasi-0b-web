<?php
    class database{
        public $host = "localhost",
        $uname = "root",
        $pass = "",
        $db = "db_penyewaan",
        $con,
        $path = "images/";
        

        public function __construct()
        {   
            $this->con = mysqli_connect($this->host,$this->uname,$this->pass,$this->db);
            date_default_timezone_set('Asia/Jakarta');
        }

        public function tampildata(){
            $sql = "SELECT m.id_member,m.nama,m.telepon,m.level, concat('http://localhost/0b_web/images/',photos) as url
                FROM member m";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        // public function tampilprodi(){
        //     $sql = "SELECT id_member,nama FROM prodi";
        //     $data = mysqli_query($this->con,$sql);
        //     while ($d = mysqli_fetch_array($data))
        //     {
        //         $hasil[] = $d;
        //     }
        //     var_dump($hasil);
        //     return $hasil;
        // }

        public function insertdata($id_member,$nm,$telepon,$level,$imstr,$file){
            $nama=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "INSERT into member(id_member, nama, telepon, level, photos) values(
                '$id_member','$nama','$telepon', '$level','$file')";
            $result=mysqli_query($this->con,$sql);
            if($result){
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $sql = "delete from member where id_member='$id_member'";
                    mysqli_query($conn,$sql);
                    $hasil['respon']="gagal";
                    return $hasil;   
                }else{
                    $hasil['respon']="sukses";
                    return $hasil;
                }
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }

        public function editdata($id_member){         // fungsi mengambil data
            $sql = "SELECT m.id_member,m.nama,m.telepon,m.level,m.photos,concat('http://localhost/0b_web/images/',photos) as url
                FROM member m
                WHERE m.id_member ='$id_member'"; 
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function updatedata($id_member,$nm,$telepon,$level,$imstr,$file){ // fungsi mengudapte data
            $nama=mysqli_real_escape_string($this->con,trim($nm));
            if($imstr==""){
                $sql = "UPDATE member SET nama='$nama', telepon='$telepon', level='$level'
                where id_member='$id_member'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //update data sukses tanpa foto
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }else{
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $hasil['respon']="gagal";
                    return $hasil;  
                }else{
                    $sql = "UPDATE member SET nama='$nama',telepon='$telepon',level='$level',photos='$file'
                            where id_member='$id_member'";
                    $result = mysqli_query($this->con,$sql);
                    if($result){
                        $hasil['respon']="sukses";
                        return $hasil; //update data sukses semua
                    }else{
                        $hasil['respon']="gagal";
                        return $hasil;
                    }
                }
            }
        }      
        public function deletedata($id_member){
            $sql = "SELECT photos from member where id_member='$id_member'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photos = $data['photos'];
                    unlink($this->path.$photos);
                }
                $sql = "DELETE from member where id_member='$id_member'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //delete data sukses
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }
        }
    }
?>