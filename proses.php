<?php 
include 'koneksi.php';
$db = new database();
 
$aksi = $_GET['aksi'];
if($aksi == "m_insert"){
    $filename = "DC_".date("YmdHis").".".pathinfo(basename($_FILES["file"]["name"]),PATHINFO_EXTENSION);
    $tempname = $_FILES['file']['tmp_name'];
 	$hasil=$db->insertdata($_POST['id_member'],$_POST['nama'],$_POST['telepon'],$_POST['level'],$tempname,$filename);
    if($hasil['respon']=="sukses"){
        header("location:member.php?pesan=insertsuccess");
    }else{
        header("location:member.php?pesan=insertfailed");
    }
}elseif($aksi == "m_update"){
    $filename = $_POST['photos'];
    $tempname = $_FILES['file']['tmp_name'];
    $hasil=$db->updatedata($_POST['id_member'],$_POST['nama'],$_POST['telepon'],$_POST['level'],$tempname,$filename);
    if($hasil['respon']=="sukses"){
        header("location:member.php?pesan=updatesuccess");
    }else{
        header("location:member.php?pesan=updatefailed");
    }
}elseif($aksi == "m_delete"){ 	
    $hasil=$db->deletedata($_GET['id_member']);
    if($hasil['respon']=="sukses"){
        header("location:member.php?pesan=deletesuccess");
    }else{
        header("location:member.php?pesan=deletefailed");
    }
}
?>