<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Data Member</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Penyewaan</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link active" href="member.php">Member <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="about.php">About</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Update Data Member</h4>
<?php foreach($db->editdata($_GET['id_member']) as $mem) : ?>
<form action="proses.php?aksi=m_update" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="id_member">ID Member</label>
            <input type="hidden" id="id_member" name="id_member" value="<?= $mem['id_member'] ?>"><br>
            <?= $mem['id_member'] ?>
        </div>
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" placeholder="Masukkan Nama" id="nama" name="nama" class="form-control" required value="<?= $mem['nama'] ?>">
        </div>
        <div class="form-group">
            <label for="telepon">Telepon</label>
            <input type="text" placeholder="Masukkan Telepon" id="telepon" name="telepon" class="form-control" required value="<?= $mem['telepon'] ?>">
        </div>
        <div class="form-group">
            <label for="level">Level</label>
            <select class="form-control" name="level">
                <option value="Silver" <?= ($mem['level']=="Silver")? "selected" : "" ?>>Silver</option>
                <option value="Gold"    <?= ($mem['level']=="Gold")? "selected" : "" ?>>Gold</option>
                <option value="Admin" <?= ($mem['level']=="Admin")? "selected" : "" ?>>Admin</option>
            </select>
        </div>
        <div class="form-group">
            <label for="photos">Foto</label><br>
            <img src="<?= $mem['url'] ?>" width="80px" height="100px" />
            <input type="hidden" id="photos" name="photos" value="<?= $mem['photos'] ?>">
            <input type="file" class="form-control-file" id="file" name="file">
        </div>

        <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
</form>
<?php endforeach ?>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>